import ArrowDown from "./ArrowDown.svelte";
import ArrowLeft from "./ArrowLeft.svelte";
import ArrowRight from "./ArrowRight.svelte";
import ArrowUp from "./ArrowUp.svelte";
import ArrowsUpdown from "./ArrowsUpdown.svelte";
import Attention from "./Attention.svelte";
import Burger from "./Burger.svelte";
import Calendar from "./Calendar.svelte";
import Cashbox from "./Cashbox.svelte";
import Cashbox2 from "./Cashbox2.svelte";
import Catalog from "./Catalog.svelte";
import Check from "./Check.svelte";
import ChevronDown from "./ChevronDown.svelte";
import ChevronLeft from "./ChevronLeft.svelte";
import ChevronRight from "./ChevronRight.svelte";
import ChevronUp from "./ChevronUp.svelte";
import Clear from "./Clear.svelte";
import Close from "./Close.svelte";
import Column from "./Column.svelte";
import Copy from "./Copy.svelte";
import Cycle from "./Cycle.svelte";
import Visible from "./Visible.svelte";
import Edit from "./Edit.svelte";
import Favorite from "./Favorite.svelte";
import FavoriteFill from "./FavoriteFill.svelte";
import File from "./File.svelte";
import Filter from "./Filter.svelte";
import History from "./History.svelte";
import InputCalendar from "./InputCalendar.svelte";
import Invisible from "./Invisible.svelte";
import Key from "./Key.svelte";
import List3 from "./List3.svelte";
import Loader from "./Loader.svelte";
import Market from "./Market.svelte";
import Message from "./Message.svelte";
import Minus from "./Minus.svelte";
import MoreHorizontal from "./MoreHorizontal.svelte";
import MoreVertical from "./MoreVertical.svelte";
import Phone from "./Phone.svelte";
import Plus from "./Plus.svelte";
import Print from "./Print.svelte";
import Question from "./Question.svelte";
import Rouble from "./Rouble.svelte";
import Save from "./Save.svelte";
import Set from "./Set.svelte";
import Settings from "./Settings.svelte";
import Sort from "./Sort.svelte";
import SortDown from "./SortDown.svelte";
import SortUp from "./SortUp.svelte";
import Trash from "./Trash.svelte";
import Upload from "./Upload.svelte";
import Download from "./Download.svelte";
import CashCheck from "./CashCheck.svelte";
import Move from "./Move.svelte";
import Tree from "./Tree.svelte";
import List4 from "./List4.svelte";
import Ok from "./Ok.svelte";
import Monitor from "./Monitor.svelte";
import ToggleOff from "./ToggleOff.svelte";
import ToggleOn from "./ToggleOn.svelte";
import Mail from "./Mail.svelte";
import MailFull from "./MailFull.svelte";
import MailOk from "./MailOk.svelte";
import FullScreen from "./FullScreen.svelte";
import SmallScreen from "./SmallScreen.svelte";
import Cart from "./Cart.svelte";
import NotifyNot from "./NotifyNot.svelte";
import Notify from "./Notify.svelte";
import MonitorNot from "./MonitorNot.svelte";
import Start from "./Start.svelte";
import Image from "./Image.svelte";
import Hub from "./Hub.svelte";
import Profile from "./Profile.svelte";
import Time from "./Time.svelte";
import Pin from "./Pin.svelte";
import Mark from "./Mark.svelte";
import CopyLink from "./CopyLink.svelte";
import Document from "./Document.svelte";
import Tire from "./Tire.svelte";
import TShirt from "./TShirt.svelte";
import Cigarette from "./Cigarette.svelte";
import Shoes from "./Shoes.svelte";
import Bike from "./Bike.svelte";
import Pulse from "./Pulse.svelte";
import Exit from "./Edit.svelte";
import ZoomIn from "./ZoomIn.svelte";
import ZoomOut from "./ZoomOut.svelte";
import Search from "./Search.svelte";

const icons = {
  "arrow-down": ArrowDown,
  "arrow-left": ArrowLeft,
  "arrow-right": ArrowRight,
  "arrow-up": ArrowUp,
  "arrows-updown": ArrowsUpdown,
  attention: Attention,
  burger: Burger,
  calendar: Calendar,
  cashbox: Cashbox,
  cashbox2: Cashbox2,
  catalog: Catalog,
  check: Check,
  "chevron-down": ChevronDown,
  "chevron-left": ChevronLeft,
  "chevron-right": ChevronRight,
  "chevron-up": ChevronUp,
  clear: Clear,
  close: Close,
  column: Column,
  copy: Copy,
  cycle: Cycle,
  visible: Visible,
  edit: Edit,
  favorite: Favorite,
  "favorite-fill": FavoriteFill,
  file: File,
  filter: Filter,
  history: History,
  "input-calendar": InputCalendar,
  invisible: Invisible,
  key: Key,
  list3: List3,
  loader: Loader,
  market: Market,
  message: Message,
  minus: Minus,
  "more-horizontal": MoreHorizontal,
  "more-vertical": MoreVertical,
  phone: Phone,
  plus: Plus,
  print: Print,
  question: Question,
  rouble: Rouble,
  save: Save,
  set: Set,
  settings: Settings,
  sort: Sort,
  "sort-down": SortDown,
  "sort-up": SortUp,
  trash: Trash,
  upload: Upload,
  download: Download,
  "cash-check": CashCheck,
  move: Move,
  tree: Tree,
  list4: List4,
  ok: Ok,
  monitor: Monitor,
  "toggle-off": ToggleOff,
  "toggle-on": ToggleOn,
  mail: Mail,
  "mail-full": MailFull,
  "mail-ok": MailOk,
  fullscreen: FullScreen,
  smallscreen: SmallScreen,
  cart: Cart,
  "notify-not": NotifyNot,
  notigy: Notify,
  "monitor-not": MonitorNot,
  start: Start,
  image: Image,
  hub: Hub,
  profile: Profile,
  time: Time,
  pin: Pin,
  mark: Mark,
  "copy-link": CopyLink,
  document: Document,
  tire: Tire,
  cigarette: Cigarette,
  "t-shirt": TShirt,
  shoes: Shoes,
  bike: Bike,
  pulse: Pulse,
  exit: Exit,
  "zoom-in": ZoomIn,
  "zoom-out": ZoomOut,
  search: Search
};

export default icons;
