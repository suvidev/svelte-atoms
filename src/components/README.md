# svelte-atoms

Svelte UI kit based on [Atol](https://www.atol.ru/) design

[Demo and docs](https://svelte-atoms.web.app/)

## Install

```
npm i svelte-atoms
```

Include variables in your project files

```
<script>
    import Variables from 'svelte-atoms/Varialbes.svelte'
</script>
<Variables>
```

## Use

```
<script>
    import Button from 'svelte-atoms/Button.svelte'
</script>
<Button>Enjoy</Button>
```
